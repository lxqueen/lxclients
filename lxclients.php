<?php defined( 'ABSPATH' ) or die( '<h1>Forbidden</h1>' );

/*
 * Plugin Name: LxClients
 * Description: Defines a client user role for WordPress.
 * Version:     1.0.0
 * Author:      Alexis Queen
 * Author URI:  http://lexi.parateam.uk
 * Text Domain: lxclients
 */


class LxClients {
    protected static $_instance = null;

    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {
        $this->init_hooks();
    }

    private function init_hooks() {
        register_activation_hook( __FILE__, array( 'LxClients', 'install' ) );

        register_deactivation_hook( __FILE__, array( 'LxClients', 'uninstall' ) );
    }

    public static function install() {
        remove_role( 'client' );

        $role_template = get_role( 'editor' );

        $role_client = add_role( 'client', 'Client', $role_template->capabilities );

        $role_caps = array(
            'edit_dashboard',
            'edit_theme_options',
            'import',
            'export',
            'list_users',
            'create_users',
            'edit_users',
            'promote_users',
            'remove_users',
            'delete_users'
        );

        foreach ( $role_caps as $new_cap ) {
            $role_client->add_cap( $new_cap );
        }
    }

    public static function uninstall() {
        remove_role( 'client' );
    }
}

function LxClients() {
    return LxClients::instance();
}

$GLOBALS[ 'lxclients' ] = LxClients();

# LxClients by Alexis Queen
**Version 1.0.0 - Defines a client user role for WordPress.**

## About
This small plugin was designed so that managed hosting providers can give their clients free reign on their website's content, without breaking options they would be otherwise granted as a full Administrator.

By default, the new role can:
 - Edit their dashboard
 - Manage the site with editorial capabilities (editing posts, pages, comments etc.)
 - Manage theme options such as widgets, menus and the Customizer
 - Create, manage, edit and delete users
 - Import and export content

They do not have the ability to activate or deactivate plugins, or change the theme (assuming they're a client, you would have built the theme specially not to be messed with!). They also cannot run any updates so they can be managed by a developer or other administrative party.

## Future plans
 - Add options page to enable or disable functionality/capabilities
 - Translations
 - World domination

## Issues
If you have any issues, please email me at <lexi AT parateam.uk>.